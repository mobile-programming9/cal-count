import 'dart:io';

void main() {
  print("MENU");
  print("Select the choice you want to perform:");
  print("1. ADD");
  print("2. SUBTRACT");
  print("3. MULTIPLY");
  print("4. DIVIDE");
  print("5. EXIT");
  print("Choice you want to enter: ");
  int? u = int.parse(stdin.readLineSync()!);
  if (u == 1) {
    print("Enter the value for x: ");
    double? x = double.parse(stdin.readLineSync()!);
    print("Enter the value for y: ");
    double? y = double.parse(stdin.readLineSync()!);
    print("Sum the two numbers is: ");
    print(x + y);
  } else if (u == 2) {
    print("Enter the value for x: ");
    double? x = double.parse(stdin.readLineSync()!);
    print("Enter the value for y: ");
    double? y = double.parse(stdin.readLineSync()!);
    print("Substract the two numbers is: ");
    print(x - y);
  } else if (u == 3) {
    print("Enter the value for x: ");
    double? x = double.parse(stdin.readLineSync()!);
    print("Enter the value for y: ");
    double? y = double.parse(stdin.readLineSync()!);
    print("Multiple the two numbers is: ");
    print(x * y);
  } else if (u == 4) {
    print("Enter the value for x: ");
    double? x = double.parse(stdin.readLineSync()!);
    print("Enter the value for y: ");
    double? y = double.parse(stdin.readLineSync()!);
    print("Divide the two numbers is: ");
    print(x / y);
  } else if (u == 5) {
    return;
  }
}